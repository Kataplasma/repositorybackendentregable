package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class UserController {

    @Autowired
    UserService userservices;


    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUser(@RequestParam(value = "order",required = false) String order){
        System.out.println("getUser");

        return new ResponseEntity<>(
                this.userservices.findAll(order),HttpStatus.OK
        );


    }


    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById (@PathVariable String id){

        System.out.println("getUserById");
        System.out.println("La Id del user a buscar es " + id);

        Optional resul = this.userservices.findById(id);

        return new ResponseEntity<>(
                resul.isPresent() ? resul.get() : "User no encontrado"
                , resul.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
    @PostMapping ("/users")
    public ResponseEntity<UserModel> addUser (@RequestBody UserModel user){
        System.out.println("addUser");
        System.out.println("La id del user que se va a crear es " + user.getId());
        System.out.println("El name del user que se va a crear es " + user.getName());
        System.out.println("La age del user que se va a crear es " + user.getAge());


        return new ResponseEntity<>(
                this.userservices.add(user) ,HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser (@RequestBody UserModel user , @PathVariable String id){
        System.out.println("updateUser");
        System.out.println("La id del user a actualizar en parametro de URL es " + id);
        System.out.println("La id del user a actualizar  es " + user.getId());
        System.out.println("El name del user a actualizar  es " + user.getName());
        System.out.println("El age del user a actualizar  es " + user.getAge());

        Optional UserUpdate = this.userservices.findById(id);

        if (UserUpdate.isPresent() == true){
            System.out.println("User  para actualizar encontrado , actualizando");
            this.userservices.update(user);
        }

        return new ResponseEntity<>(
                user
                , UserUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity<String> deleteUser (@PathVariable String id){
        System.out.println("deleteUser");
        System.out.println("El id del user a eliminar es " +id);

        boolean deletedUser = this.userservices.delete(id);

        return new ResponseEntity<>(
                deletedUser ? "User Eliminado" : "User no encontrado"
                , deletedUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
