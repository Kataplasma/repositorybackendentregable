package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll(String order){
        System.out.println("En findAll de UserServices ");

        List <UserModel> listFindUser = null;

        if (order == null){
            System.out.println("Sin Ordenamiento Enviado ");
            listFindUser = this.userRepository.findAll();
        }
        else {
            if (order.equals("asc")) {
                System.out.println("Ordenamiento seleccionado en Ascendente");
                listFindUser = this.userRepository.findAll(Sort.by("age").ascending());
            }
            if (order.equals("desc")) {
                System.out.println("Ordenamiento seleccionado en Descendente");
                listFindUser = this.userRepository.findAll(Sort.by("age").descending());
            }
            else if (!(order.equals("desc"))&&!(order.equals("asc"))){
                System.out.println("Ordenamiento seleccionado No Valido ");
                listFindUser = this.userRepository.findAll();
            }
        }
        return listFindUser;
    }


    public Optional findById (String id){
        System.out.println("En findById de UserServices");

        Optional result = this.userRepository.findById(id);

        return result;
    }

    public UserModel add (UserModel user){
        System.out.println("En addUser de UserServices");

        return this.userRepository.save(user);
    }
    public UserModel update (UserModel user){
        System.out.println("En update de UserServices");

        return this.userRepository.save(user);
    }

    public boolean delete (String id){
        System.out.println("En delete de UserServices");

        boolean result = false;

        if (this.findById(id).isPresent() ==true){
            System.out.println("User Encontrado, Borrando");
            this.userRepository.deleteById(id);
            result = true;
        }
        return result;
    }
}
