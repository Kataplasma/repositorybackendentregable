package com.techu.apitechu.controllers;

import com.techu.apitechu.ApitechuApplication;
import com.techu.apitechu.models.ProductModel;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
public class ProductController {
    static final String APIBaseUrl = "/apitechu/v1";

    @GetMapping(APIBaseUrl + "/products")
    public ArrayList<ProductModel> getProducts() {
        System.out.println("getProduct");

        return ApitechuApplication.productModels;
    }

    @GetMapping(APIBaseUrl + "/products/{id}")
    public ProductModel getProductById(@PathVariable String id) {
        System.out.println("getProductById");
        System.out.println("id es :" + id);

        ProductModel result = new ProductModel();

        for (ProductModel product : ApitechuApplication.productModels) {
            if (product.getId().equals(id)) {
                result = product;
            }
        }

        return result;
    }

    @PostMapping(APIBaseUrl + "/products")
    public ProductModel createProduct(@RequestBody ProductModel newProduct) {
        System.out.println("createProduct");
        System.out.println("La id del nuevo producto es " + newProduct.getId());
        System.out.println("La descripcion del nuevo producto es " + newProduct.getDesc());
        System.out.println("El precio  del nuevo producto es " + newProduct.getPrice());

        ApitechuApplication.productModels.add(newProduct);
        return newProduct;
    }

    @PutMapping(APIBaseUrl + "/products/{id}")
    public ProductModel updateProduct(
            @RequestBody ProductModel product, @PathVariable String id
    ) {
        System.out.println("updateProduct");
        System.out.println("El id del producto  a actualizar  en parámetro de URL es  " + id);
        System.out.println("La id del producto a actualizar es " + product.getId());
        System.out.println("La descripción del producto a actualizar es " + product.getDesc());
        System.out.println("El precio  del  producto  a actualziar es " + product.getPrice());


        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                productInList.setId(product.getId());
                productInList.setDesc(product.getDesc());
                productInList.setPrice(product.getPrice());
            }
        }

        return product;
    }

    @DeleteMapping(APIBaseUrl + "/products/{id}")
    public ProductModel deleteProduct(@PathVariable String id) {
        System.out.println("DeleteProduct");
        System.out.println("La id del producto a borrrar es " + id);

        ProductModel result = new ProductModel();
        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto Encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true) {
            System.out.println("Borrando producto");
            ApitechuApplication.productModels.remove(result);
        }
        return result;

    }

/*    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel patchProduct(
            @RequestBody ProductModel product, @PathVariable String id
    ) {
        ProductModel result = new ProductModel();
        System.out.println("patchProduct");
        System.out.println("El id del producto  a actualizar  en parámetro de URL es  " + id);

        boolean foundCompany = false;

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                System.out.println("Producto Encontrado");
                foundCompany = true;
                result = productInList;
            }
        }

        if (foundCompany == true) {
            ApitechuApplication.productModels.remove(result);
            if (result.getPrice() != product.getPrice() && product.getPrice() > 0.0) {
                System.out.println("El precio  del  producto  a actualziar es " + product.getPrice());
                result.setPrice(product.getPrice());
            } else {
                result.setPrice(result.getPrice());
            }
            if (!(result.getDesc().equals(product.getDesc())) && product.getDesc() != null) {
                System.out.println("La descripción del producto a actualizar es " + product.getDesc());
                result.setDesc(product.getDesc());
            } else {
                result.setDesc(result.getDesc());
            }
            ApitechuApplication.productModels.add(result);
        } else {
            System.out.println("Producto  No Encontrado");
        }

        return  result;
    }*/

    @PatchMapping(APIBaseUrl + "/products/{id}")
    public ProductModel pathProduct(@RequestBody ProductModel productData, @PathVariable String id){
        System.out.println("patchProduct ");
        System.out.println("El id del producto a actualizar es " + id);
        System.out.println("La descripcion del producto a actualziar es "  + productData.getDesc());
        System.out.println("El precio del producto a actualizar es " + productData.getPrice());

        ProductModel result = new ProductModel();

        for (ProductModel productInList : ApitechuApplication.productModels) {
            if (productInList.getId().equals(id)) {
                result = productInList;
                if (productData.getDesc() != null){
                    System.out.println("Actulizando  la descripcion del producto");
                    productInList.setDesc(productData.getDesc());
                }
                if(productData.getPrice() > 0){
                    System.out.println("Actulizando  la descripcion del  precio");
                    productInList.setPrice(productData.getPrice());
                }
            }
        }
        return result;
    }
}

