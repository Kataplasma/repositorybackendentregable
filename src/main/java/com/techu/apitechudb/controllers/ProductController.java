package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2" )
public class ProductController {

    @Autowired
    ProductServices productServices;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts(){
        System.out.println("getProducts");

       return new ResponseEntity<>(
               this.productServices.findAll(), HttpStatus.OK
        );
    }

    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id){
        System.out.println("getProductById");
        System.out.println("El Id del producto a buscar es " + id);

        Optional <ProductModel> result = this.productServices.findById(id);



        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ?  HttpStatus.OK : HttpStatus.NOT_FOUND
        );

        // x = (condición) ? vale_esto_si_true : vale_esto_si_false

        //if (result.isPresent() == true){
            //La caja esta llena, puedo hacer get.

        //}
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product){
        System.out.println("addProduct");
        System.out.println("La id  del producto que se va a crear es " + product.getId());
        System.out.println("La desc del producto que se va a crear es " + product.getDesc());
        System.out.println("El precio del producto que se va a crear es " + product.getPrice());

        return new ResponseEntity<>(
                this.productServices.add(product)
                , HttpStatus.CREATED
        );

    }

    @PutMapping("/products/{id}")
    public ResponseEntity<ProductModel> updateProduct(@RequestBody ProductModel product , @PathVariable String id ){
        System.out.println("UpdateProduct");
        System.out.println("La id  del producto a actualizar en parámetro URL es " + id);
        System.out.println("La id  del producto a actualizar  es " + product.getId());
        System.out.println("La descripción  del producto a actualizar  es " + product.getDesc());
        System.out.println("El precio  del producto a actualizar  es " + product.getPrice());

        Optional<ProductModel> productUpdate = this.productServices.findById(id);

        if (productUpdate.isPresent()){
            System.out.println("Producto para actualizar encontrado, actualizando");
            this.productServices.update(product);
        }

        return  new ResponseEntity<>(
                product
                , productUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity<String> deleteProduct(@PathVariable String id){
        System.out.println("deleteProduct");

        boolean deletedproduct = this.productServices.delete(id);


        return new ResponseEntity<>(
                deletedproduct ? "Producto Borrado" : "Producto no borrado",
                deletedproduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );


    }
}
